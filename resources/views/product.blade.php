@extends('master')
@section('content')
<!-- start carousal -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  	@foreach($products as $p)
    <div class="item {{$p['id']==1? 'active':'' }}">
      <img src="{{$p['gallery']}}"  alt="...">
      <div class="carousel-caption">
        Caption 1
      </div>
    </div>
    @endforeach

    
    Below text
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<!-- End carousal -->


<!-- Start Product view -->

<div class="row">
	@foreach($products as $p)
	<div class="col-sm-3">
		<div class="thumbnail">
			<img src="{{$p['gallery']}}" alt="...">
			<div class="caption">
				<a href="detail/{{$p['id']}}"><h3>{{$p['name']}}</h3></a>
				<p>{{$p['description']}}</p>
				<p><a href="#" class="btn btn-primary" role="button">Price: {{$p['price']}}</a></p>

					
			</div>
		</div>
	</div>
	@endforeach
</div>


<!-- End Product view -->

@endsection