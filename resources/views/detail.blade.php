@extends('master')
@section('content')



<!-- Start Product view -->

<div class="row">

	<div class="col-sm-3">
		<div class="thumbnail">
			<img src="{{$product['gallery']}}" alt="...">
			<div class="caption">
				<a href="detail/{{$product['id']}}"><h3>{{$product['name']}}</h3></a>
				<p> Description:{{$product['description']}}</p>
        <p> Category:{{$product['category']}}</p>
				<p><a href="#" class="btn btn-primary" role="button">Price: {{$product['price']}}</a></p>
        
        <form action="/add_to_cart" method="POST">
          @csrf          
          <input type="hidden" value="{{$product['id']}}" name="product_id">
          <input type="submit"  class="btn btn-primary" value="add to cart">
        </form>
					
			</div>
		</div>
	</div>

</div>


<!-- End Product view -->

@endsection