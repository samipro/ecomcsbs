@extends('master')
@section("content")
<div class="custom-product">
     <div class="col-sm-4">
         <a href="#">Cartlist</a>
     </div>
     <div class="col-sm-4">
        <div class="trending-wrapper">
            <h4>Cart list</h4>
            @foreach($products as $item)
            <div class="searched-item">
              <a href="detail/{{$item->id}}">
              <img class="trending-image" src="{{$item->gallery}}">
              <div class="">
                <h2>{{$item->name}}</h2>
                <h5>{{$item->description}}</h5>
              </div>
              <a href="removecart/{{$item->cart_id}}" class="btn btn-primary">remove</a>
            </a>
            </div>
            <hr>
            @endforeach
          </div>
          <a href="ordernow" class="btn btn-primary jumbotron">Order now</a>
     </div>



  
         
 

</div>

@endsection