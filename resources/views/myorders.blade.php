<!-- @foreach($products as $p)
{{$p->id}} <br>
{{$p->name}}<br>
{{$p->price}}<br>
{{$p->category}} <br>
{{$p->address}} <br>
{{$p->gallery}} <br>
@endforeach
 -->


@extends('master')
@section("content")
<div class="custom-product">
     <div class="col-sm-4">
         <a href="#">My Orders</a>
     </div>
     <div class="col-sm-4">
        <div class="trending-wrapper">
            <h4>My Order</h4>
            @foreach($products as $item)
            <div class="searched-item">
              <a href="detail/{{$item->id}}">
              <img class="trending-image" src="{{$item->gallery}}">
              <div class="">
                <h2>{{$item->name}}</h2>
                <h5>{{$item->description}}</h5>
                Price:<h5>{{$item->price}}</h5>
                Address:<h5>{{$item->address}}</h5>
              </div>
             
            </a>
            </div>
            <hr>
            @endforeach
          </div>
     </div>



  
         
 

</div>

@endsection