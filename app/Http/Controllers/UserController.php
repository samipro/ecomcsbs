<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;

class UserController extends Controller
{
    public function login(Request $req){
    	$user=User::where('email',$req->email)->first();
    	if(!$user || Hash::check($user->password,$req->password)){
    		return "Wrong Username or password";
    	}else{
    		Session::put('user',$user);
    		return redirect('/');
    	}
    	
    }
}
