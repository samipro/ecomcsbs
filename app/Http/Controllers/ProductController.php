<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Order;
use Session;
use DB;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
    	$data=Product::all();
    	return view('product',['products'=>$data]);
    }

    public function detail($id){
    	$item=Product::find($id);

    	return view('detail',['product'=>$item]);
    }

    public function addToCart(Request $req){
    	if(Session::has('user')){
    		$cart=new Cart;
    		$cart->product_id=$req->product_id;
    		$cart->user_id=Session::get('user')['id'];
    		$cart->save();
            return redirect('/');
        }else{
          return redirect('/login');
      }    	

      
  }

  static public function cartItem(){
    if(Session::has('user')){

        return Cart::where('user_id',Session::get('user')['id'])->count();
    }else 
    return 0;
    
}

public function search(Request $req){
    
    $data=Product::where('name','like','%'.$req->search_item.'%')->get();

    return view('search',['products'=>$data]);
}

public function cartList(){
    if(Session::has('user')){
        $uid=Session::get('user')['id'];
            //$data=Cart::where('user_id',Session::get('user')['id'])->get();
/*            $data=DB::table('cart')->join('users','cart.user_id','=','users.id')
            ->where('cart.user_id',$uid)
            ->join('products','cart.product_id','=','products.id')
            ->select('products.*')
            ->get();*/

            $data=DB::table('cart')
            ->join('products','cart.product_id','=','products.id')
            ->where('cart.user_id',$uid)
            ->select('products.*','cart.id as cart_id')
            ->get();

            return view('cartlist',['products'=>$data]);
        }
    }



    public function removeCart($id){
        Cart::find($id)->delete();
        return redirect('cartlist');
    }

    public function ordernow(){
        $uid=Session::get('user')['id'];
        $price_total=DB::table('cart')
        ->join('products','cart.product_id','=','products.id')
        ->where('cart.user_id',$uid)
        ->sum('products.price');
        //return $price_total;
        return view('ordernow',['price_total'=>$price_total]);
    }

    public function orderPlace(Request $req){
                $uid=Session::get('user')['id'];
                $all_cart=Cart::where('user_id',$uid)->get();
               // return $all_cart;
                foreach ($all_cart as $cart) {
                    $order=new Order;
                    $order->product_id=$cart['product_id'];
                    $order->user_id=$cart['user_id'];
                    $order->status="pending";
                    $order->payment_method="cash";
                    $order->payment_status="cash on delivery";
                    $order->address=$req->address;
                    $order->save();
                    Cart::destroy($cart['id']);

                }

                 

                 return redirect('/');        
           
        }

        public function myOrders(){
                $uid=Session::get('user')['id'];
                $data=DB::table('orders')
                ->join('products','orders.product_id','=','products.id')
                ->select('products.*','orders.address')
                ->get();
                //return $data;
                return view('myOrders',['products'=>$data]);
        }

       
        
    


}

